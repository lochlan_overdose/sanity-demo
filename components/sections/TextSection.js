import { PortableText } from "../../utils/sanity";

function TextSection(props) {
  const { text, heading } = props;
  return (
    <div>
      <div className="container mx-auto p-20">
        <div className="md:flex md:items-center md:flex-col text-center">

          <h3 className="text-gray-700 uppercase text-lg">{heading}</h3>
          {text && <PortableText blocks={text} />}
        </div>
      </div>
    </div>
  );
}

export default TextSection;
