export { default as Testimonial } from "./Testimonial";
export { default as OurTeam } from "./OurTeam";
export { default as Gallery } from "./Gallery";
export { default as Hero } from "./Hero";
export { default as TextSection } from "./TextSection";
export { default as ImageSection } from "./ImageSection";
