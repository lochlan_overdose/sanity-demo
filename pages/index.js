import Error from 'next/error'
import { groq } from 'next-sanity'
import { useRouter } from 'next/router'
import LandingPage from '../components/LandingPage'
import { getClient, usePreviewSubscription } from '../utils/sanity'

const query = groq`*[_type == "homePage"][0]`

function HomePageContainer ({ pageData, preview }) {
  const router = useRouter()

  if (!router.isFallback && !pageData) {
    return <Error statusCode={404} />
  }

  const { data } =  usePreviewSubscription(query, {
    params: {  },
    initialData: pageData,
    enabled: preview || router.query.preview !== null
  })

  console.log({ data })

  return <LandingPage page={data} />
}

export async function getStaticProps ({ params = {}, preview = false }) {
  const {  } = params
  const pageData = await getClient(preview).fetch(query, {

  })

  return {
    props: { preview, pageData }
  }
}

export default HomePageContainer
