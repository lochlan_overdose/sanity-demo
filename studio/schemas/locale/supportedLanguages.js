export default [
  { id: "en_AU", title: "Australia (English)", isDefault: true },
  { id: "en_US", title: "United States (English)" },
  { id: "en_CA", title: "Canada (English)" },
  { id: "fr_CA", title: "Canada (French)" }
];
