export default {
  title: "Gallery",
  name: "gallery",
  type: "object",
  fields: [
    {
      title: "Title",
      name: "title",
      type: "string",
    }
  ],
  preview: {
    select: {
      title: "title",
    },
    prepare({ title }) {
      return {
        title: `${title}`
      };
    },
  },
};
