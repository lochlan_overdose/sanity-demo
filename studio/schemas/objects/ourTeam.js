export default {
  title: "Our Team",
  name: "ourTeam",
  type: "object",
  fields: [
    {
      title: "Title",
      name: "title",
      type: "string",
    }
  ],
  preview: {
    select: {
      title: "title",
    },
    prepare({ title }) {
      return {
        title: `${title}`
      };
    },
  },
};
