import React from "react";
import styles from "../IframePreview.css";

export default function HomePagePreview(props) {

  const url =
    process.env.NODE_ENV === "production"
      ? `../../?preview`
      : `http://localhost:3000?preview`;

  return (
    <div className={styles.componentWrapper}>
      <div className={styles.iframeContainer}>
        <iframe src={url} frameBorder={"0"} />
      </div>
    </div>
  );
}
